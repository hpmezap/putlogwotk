const express = require('express'); //referencia al paquete express
const userfile = require('./user.json');
const URL_BASE = '/api-peru/v1/';

var app = express();
var port = process.env.PORT || 3000;

const bodyParser = require('body-parser');

app.use(bodyParser.json());
//http://localhost:3000/api-peru/v1/users
app.get(URL_BASE + 'users',
   function(request,response) {
     response.status(200);
     response.send(userfile);
});
//http://localhost:3000/api-peru/v1/users/11
app.get(URL_BASE + 'users/:id',
   function(request,response) {
     console.log(request.query);
     let indice = request.params.id;
     let respuesta =
       (userfile[indice-1] == undefined) ? {"msg":"No existe"} : userfile[indice-1]
       response.status(200).send(respuesta);
});
// en el post del postman parte body debemos crear un usuario del get
app.post(URL_BASE + 'users',
 function (req,res){
   console.log(req.body);
   console.log(req.body.id);
   let pos = userfile.length;
   let newUser = {
 //  id: req.body.id,
     id: ++pos,
     first_name: req.body.first_name,
     last_name: req.body.last_name,
     email: req.body.email,
     password: req.body.password
   }
   userfile.push(newUser);
   res.status(201);
   res.send({"mensaje":"Usuario creado correctamente",
             "Usuario":newUser,
             "userfile actualizado":userfile});
 });
// instruccion delete -- userfile.splice elimina el elemento de posicion,
// 1 borrado de la posicion 1 registro; si colocas 2 borrara dos registros

app.delete(URL_BASE + 'users/:id',
function(request,response) {
// console.log(request.query);
let indice = request.params.id;
let respuesta =
(userfile[indice-1] == undefined) ?
{"msg":"No existe"} :
{"Usuario eliminado" : (userfile.splice(indice-1,2))}
response.status(200).send(respuesta);
});

app.put(URL_BASE + 'users/:id',
function(request,response) {
  let indice = request.params.id;
  let updUser = {
    id: eval(indice),
    first_name: request.body.first_name,
    last_name: request.body.last_name,
    email: request.body.email,
    password: request.body.password
  };
//  userfile.splice(indice-1,1,updUser);
  let respuesta =
   (userfile[indice-1] == undefined) ? {"msg":"No existe"} :   userfile.splice(indice-1,1,updUser);
    response.status(201).send(respuesta);
/*
    (userfile[indice-1] == undefined) ? {"msg":"No existe"} : userfile[indice-1]

 let respuesta = userfile.splice(indice-1,1,updUser);
  response.status(201);
  response.send({"mensaje":"Usuario modificado correctamente",
            "Usuario":updUser,
            "userfile actualizado":userfile});
*/
});

// LOGIN - user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userfile) {
    if(us.email == user) {
     if(us.password == pass) {
      us.logged = true;
      writeUserDataToFile(userfile);
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.",
              "idUsuario" : us.id,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
     }
    }
   }
 });
// LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var userId = request.body.id;
   for(us of userfile) {
    if(us.id == userId) {
     if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userfile);
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
     } else {
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
     }
    }
   }
 });
 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en 'user.json'.");
    }
   })
 }

app.listen(port, function() {
  console.log('Node JS escuchando en el puerto 3000...');
  console.log('Hola Patty...');
  console.log('Hola Luis...');
});
